var proxy = require('http-proxy-middleware')
const path = require('path')

module.exports = {
  siteMetadata: {
    title: 'Blockstack Starter',
    description: `Kick off your next, great Gatsby app.`,
    author: `@friedger`,
  },
  developMiddleware: app => {
    app.use(
      '/.netlify/functions/',
      proxy({
        target: 'http://localhost:9000',
        pathRewrite: {
          '/.netlify/functions/': '/',
        },
      })
    )
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-create-client-paths`,
      options: { prefixes: [`/app/*`] },
    },
    `gatsby-plugin-sass`,
    {
      resolve: 'gatsby-plugin-root-import',
      options: {
        src: path.join(__dirname, 'src'),
        api: path.join(__dirname, 'src/api'),
        components: path.join(__dirname, 'src/components'),
        // pages: path.join(__dirname, 'src/pages'),
        utils: path.join(__dirname, 'src/utils'),
        style: path.join(__dirname, 'src/style'),
        reducers: path.join(__dirname, 'src/reducers'),
      },
    },
    // {
    //   resolve: `gatsby-plugin-manifest`,
    //   options: {
    //     name: `Cozy Reader`,
    //     short_name: `Cozy`,
    //     start_url: `/app`,
    //     background_color: `#663399`,
    //     // theme_color: `#663399`,
    //     display: `minimal-ui`,
    //     icon: `/favicons/android-chrome-512x512.png`, // This path is relative to the root of the site.
    //   },
    // },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.app/offline
    // 'gatsby-plugin-offline',
  ],
}
