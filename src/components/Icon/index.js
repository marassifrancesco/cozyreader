import React from "react";
import { ReactComponent as BarChart } from "style/icons/barChart.svg";
import { ReactComponent as Settings } from "style/icons/settings.svg";
import { ReactComponent as AddMonitor } from "style/icons/addMonitor.svg";
import { ReactComponent as Close } from "style/icons/close.svg";
import { ReactComponent as Download } from "style/icons/download.svg";
import { ReactComponent as Launch } from "style/icons/launch.svg";
import { ReactComponent as Arrow } from "style/icons/arrow.svg";
import { ReactComponent as ArrowLeft } from "style/icons/arrowLeft.svg";
import { ReactComponent as ArrowRight } from "style/icons/arrowRight.svg";
import { ReactComponent as ArrowDown } from "style/icons/arrowDown.svg";
import { ReactComponent as Back } from "style/icons/back.svg";
import { ReactComponent as Flag } from "style/icons/flag.svg";
import { ReactComponent as Current } from "style/icons/current.svg";

const icons = {
  BarChart,
  Settings,
  AddMonitor,
  Close,
  Download,
  Launch,
  Arrow,
  ArrowLeft,
  ArrowRight,
  ArrowDown,
  Current,
  Back,
  Flag
};
const Icon = ({ name, className, style, size, onClick }) => {
  const Component = icons[name];
  return <Component onClick={onClick} className={className} style={style} />;
};

export default Icon;
