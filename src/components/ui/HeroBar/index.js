import React from 'react';
import { UpTitle, Title } from 'components/ui/Typography';
import css from './style.module.scss';

export const HeroBar = ({ upTitle, title, children }) => (
  <div className={css.heroBar}>{children}</div>
);

export default HeroBar;
