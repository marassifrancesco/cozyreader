function addToCozy() {
  const WEB_APP_URL = 'https://cozyreader.xyz/browser/extension/index.html'
  const WEB_APP_ORIGIN = 'https://cozyreader.xyz/'
  const iframe = document.createElement('iframe')
  const closeButton = document.createElement('span')
  closeButton.id = 'cozy-close-button'
  closeButton.style.position = 'fixed'
  closeButton.style.top = '16px'
  closeButton.style.right = '16px'
  closeButton.style.zIndex = '9999999999'
  closeButton.style.height = '40px'
  closeButton.style.width = '40px'
  closeButton.style.backgroundColor = '#fff'
  closeButton.style.fontSize = '16px'
  closeButton.style.lineHeight = '20px'
  closeButton.style.textAlign = 'center'
  closeButton.style.cursor = 'pointer'
  closeButton.style.borderRadius = '999em'
  closeButton.style.alignItems = 'center'
  closeButton.style.justifyContent = 'center'
  closeButton.innerHTML = '&#10005;'
  closeButton.style.display = 'none'
  const initFrame = url => {
    iframe.id = 'cozy-parent-iframe'
    iframe.style.background = 'transparent'
    iframe.style.height = '600px'
    iframe.style.width = '400px'
    iframe.style.position = 'fixed'
    iframe.style.top = '0px'
    iframe.style.right = '0px'
    iframe.style.zIndex = '9999999998'
    iframe.frameBorder = 'none'
    iframe.style.border = 'none'
    iframe.style.borderRadius = '2px'
    iframe.style.backgroundColor = '#fafafa'
    iframe.style.boxShadow = '0 1px 6px 0 rgba(45,147,173,0.17)'
    closeButton.style.display = 'flex'

    iframe.src = WEB_APP_URL
    document.body.appendChild(iframe)
    document.body.appendChild(closeButton)

    document
      .getElementById('cozy-close-button')
      .addEventListener('click', () => {
        removeFrameElements()
      })

    let iframeEl = document.getElementById('cozy-parent-iframe')
    if (iframeEl) {
      let timer = setInterval(() => {
        iframeEl = document.getElementById('cozy-parent-iframe')
        if (!iframeEl || !iframeEl.contentWindow) {
          clearInterval(timer)
        }
        iframeEl &&
          iframeEl.contentWindow &&
          iframeEl.contentWindow.postMessage(url, WEB_APP_ORIGIN)
      }, 1000)
    }
    window.addEventListener('message', event => close(event), false)
  }

  const removeFrameElements = () => {
    if (document.getElementById('cozy-parent-iframe'))
      document.getElementById('cozy-parent-iframe').remove()
    if (document.getElementById('cozy-close-button'))
      document.getElementById('cozy-close-button').remove()
  }

  const close = event => {
    if (event.origin !== WEB_APP_ORIGIN) return
    removeFrameElements()
  }
  initFrame(window.location.href)
}
addToCozy()
