const Mercury = require('@postlight/mercury-parser')
const md5 = require('md5')

exports.handler = function(event, context, callback) {
  const url = event.queryStringParameters.url
  Mercury.parse(url).then(result => {
    const output = {
      title: result.title,
      author: result.author,
      published: result.date_published,
      image: result.lead_image_url,
      wordCount: result.word_count,
      domain: result.domain,
      description: result.excerpt,
      id: md5(url),
      createdAt: new Date(),
      url,
      content: result.content.replace(/srcset/g, '_'),
    }
    callback(null, {
      statusCode: 200,
      body: JSON.stringify(output),
    })
  })
}
